import React, { useState } from "react";

const Image = () => {
  const [isHover, setIsHovered] = useState(false);
  return (
    <>
      <div
        onMouseEnter={() => {
          console.log("true");
          setIsHovered(true);
        }}
        onMouseLeave={() => {
          console.log("false");
          setIsHovered(false);
        }}
      >
        {isHover ? (
          <img
            style={{ width: "180px", height: "115px" }}
            src="https://cdn.pixabay.com/photo/2015/03/10/17/23/youtube-667451_1280.png"
          />
        ) : (
          <img
            style={{ width: "100px", height: "80px" }}
            src="https://cdn.pixabay.com/photo/2015/03/10/17/23/youtube-667451_1280.png"
          />
        )}
      </div>
      <div>
        <button
          onDoubleClick={() => {
            alert("Button Pressed twice");
            console.log("Button Pressed");
            // window.print();
          }}
        >
          Double Click
        </button>
      </div>
      <div>
        <input
          type="text"
          placeholder="Enter text"
          onFocus={() => console.log("Focused on the textBox")}
          onBlur={() => console.log("Out of textBox")}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              console.log("Enter key Pressed");
            }
          }}
        />
      </div>
      <div>
        <p
          onCopy={() => alert("Something is copying")}
          onCut={() => alert("Something is Cut")}
          onPaste={() => alert("Something is Paste")}
        >
          Hey.. I am a MERN Developer
        </p>
        <button onClick={() => window.close()}>Close</button>
      </div>
    </>
  );
};

export default Image;
